# Cervical cancer identification


_Michał  Kruczkowski,  Anna  Drabik-Kruczkowska,  Anna  Marciniak,  Martyna Tarczewska, Monika Kosowska and Małgorzata Szczerska_

Machine  learning  for  predictions  of  cervical cancer identification – preliminary investigation based on refractive index

The best option to run this code is to upload notebook and all unpacked data (0.CSV, folder 1550-srebro and df_cancer.csv) to Google Colaboratory, that way there are no additional steps required.
If you want to run it in your local environment you should install following packeges:

- seaborn
- pandas
- numpy
- tensorflow
- matplotlib
- scipy
- sklearn
